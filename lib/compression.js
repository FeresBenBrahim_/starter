const sharp = require("sharp");

module.exports = (file,pipelines = []) => {
    const newFileName = "/compressed-" + file.filename;;
    const compressedImagePath  = file.destination + newFileName;
    return new Promise((resolve,reject) => {
        const sh = sharp(file.path);
        for(const opt of pipelines){
            if(opt.resize)
                sh.resize(opt.resize.width,opt.resize.height)
            if(opt.rotate){
                sh.rotate(opt.rotate)
            }
        }
        sh
        .jpeg({ quality: 80 }) // Adjust the quality (0 to 100) as needed
        .toFile(compressedImagePath, (err, info) => {
            if (err) {
                return reject(err)
            } else {
                resolve({...info,
                    location:process.env.SERVER + newFileName
                });
            }
        });
    })
}

