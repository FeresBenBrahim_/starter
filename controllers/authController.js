const userModel = require('../models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { promisify } = require('util');
const crypto = require('crypto');
const asyncHandler = require('express-async-handler');
const {
  BadRequestError,
  UnauthorizedError,
  AccessTokenError,
  TokenExpiredError,
  NotFoundError,
} = require("../middlewares/apiError");
const {
  SuccessResponse,
  SuccessMsgDataResponse,
  SuccessMsgResponse,
} = require("../middlewares/apiResponse");
const cloudinary = require("../utils/cloudinary");
const upload = require("../utils/multer");
const tranEmailApi = require("../utils/email");
const uploadFile = upload.single("file");
let refreshTokens = [];
exports.uploadFile = uploadFile;

const signToken = (id) => {
  console.log("process env ",process.env)
  return jwt.sign({ id }, process.env.SECRET, {
    expiresIn: process.env.ExpiresIn,
  });
};
const signRefreshToken = (id) => {
  return jwt.sign({ id }, 'super-secret-t', {
    expiresIn: 1 * 24 * 60 * 60 * 1000,
  });
};
const createSendToken = (user, res) => {
  console.log('create token');
  const token = signToken(user._id);
  console.log('sign in yes');

  const refreshToken = signRefreshToken(user._id);
  refreshTokens.push(refreshToken);

  console.log('signRefreshToken yes');
  const cookieOptions = {
    expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
    httpOnly: true,
  };
  res.cookie('jwt', token, cookieOptions);
  console.log('cookies set yes');
  user.password = undefined;
  return new SuccessResponse({ token, refreshToken, user }).send(res);
};

exports.login = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  console.log('login api : ', email, ' - password : ', password);
  const user = await userModel.findOne({ email }).select('password');
  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw new BadRequestError('Incorrect email or password');
  }

  createSendToken(user, res);
});

exports.signUp = asyncHandler(async (req, res) => {
  try {
    const checkUser = await userModel.findOne({ email: req.body.email });
    if (checkUser) {
      throw new BadRequestError('A user with this email already exists');
    }
    if (req.file) {
      const result = await cloudinary.uploader.upload(req.file.path);
      req.body.avatar = result.secure_url || '';
      req.body.cloudinaryID = result.public_id || '';
    }

    if (req.body.role) {
      delete req.body.role;
    }
    const user = await userModel.create(req.body);

    const token = signToken(user._id);

    return new SuccessResponse({
      user,
      token,
    }).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});


exports.createAdmin = asyncHandler(async (req, res) => {
  try {
    const checkUser = await userModel.findOne({ email: req.body.email });
    if (checkUser) {
      throw new BadRequestError("A user with this email already exists");
    }
    if (req.file) {
      const result = await cloudinary.uploader.upload(req.file.path);
      req.body.avatar = result.secure_url || "";
      req.body.cloudinaryID = result.public_id || "";
    }

    req.body.role = "admin"
    const user = await userModel.create(req.body);

    const token = signToken(user._id);

    return new SuccessResponse({
      user,
      token,
    }).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.getMe = asyncHandler(async (req, res) => {
  try {
    let token;
    if (req.headers.authorization) {
      token = req.headers.authorization.split(' ')[1];
    }
    if (!token) {
      throw new AccessTokenError('No token provided');
    }
    const decoded = await promisify(jwt.verify)(token, process.env.SECRET);
    const freshUser = await userModel.findById(decoded.id);

    if (!freshUser) {
      throw new TokenExpiredError("Token expired");
    }
    return new SuccessResponse(freshUser).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.protect = asyncHandler(async (req, res, next) => {
  let token;

  if (req.headers.authorization) {
    token = req.headers.authorization.split(' ')[1];
  }
  if (!token) {
    throw new AccessTokenError('No token provided');
  }
  try {
    const decoded = await promisify(jwt.verify)(token, process.env.SECRET);
    const freshUser = await userModel.findById(decoded.id);
    if (!freshUser) {
      throw new UnauthorizedError('The user belonging to this token does no longer exist.');
    }

    req.user = freshUser;

    next();
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      throw new UnauthorizedError('You do not have permission to perform this action');
    }
    next();
  };
};

exports.updatePassword = asyncHandler(async (req, res) => {
  const user = await userModel.findById(req.user._id).select('password');
  if (!user) {
    throw new NotFoundError('User not found');
  }
  if (!(await user.correctPassword(req.body.currentPassword, user.password))) {
    throw new BadRequestError('Your current password is wrong');
  }
  user.password = req.body.newPassword;
  user.passwordConfirm = req.body.newPasswordConfirm;
  await user.save();
  const token = signToken(user._id);

  return new SuccessMsgDataResponse(token, 'Password updated sucessfully').send(res);
});

exports.updateMe = asyncHandler(async (req, res) => {
  const user = await userModel.findById(req.user?._id);
  if (!user) {
    throw new BadRequestError('User not found');
  }
  if (req.file && user?.avatar !== null) {
    await cloudinary.uploader.destroy(user?.cloudinaryID);
    const result = await cloudinary.uploader.upload(req.file.path);
    req.body.avatar = result?.secure_url || '';
    req.body.cloudinaryID = result?.public_id || '';
  }
  if (req.file && user?.avatar === null) {
    const result = await cloudinary.uploader.upload(req.file.path);
    req.body.avatar = result?.secure_url || '';
    req.body.cloudinaryID = result?.public_id || '';
  }
  const { email } = req.body;
  const checkUser = await userModel.findOne({ email });
  if (checkUser && checkUser.email !== req.user?.email) {
    throw new BadRequestError('A user with this email already exists');
  }
  if (req.body.role) {
    delete req.body.role;
  }

  const updatedUser = await userModel.findByIdAndUpdate(
    req.user._id,
    { $set: req.body },
    { new: true }
  );
  return new SuccessMsgDataResponse(updatedUser, 'Profile updated sucessfully').send(res);
});

exports.forgotPassword = asyncHandler(async (req, res, next) => {
  const { email } = req.body;
  const user = await userModel.findOne({ email });
  if (!user) {
    throw new BadRequestError('No user found with this email');
  }

  const resetToken = user.createPasswordResetToken();
  await user.save({ validateBeforeSave: false });

  const sender = {
    email: process.env.SENDER,
    name: process.env.NAME,
  };

  const receivers = [
    {
      email: email,
    },
  ];

  tranEmailApi
    .sendTransacEmail({
      sender,
      to: receivers,
      subject: 'Password reset',
      htmlContent: `
          <h4>If you forgot your password please use this code to reset it: </h4> <h1> ${resetToken}</h1>
                  `,
    })
    .then(() => {
      return new SuccessMsgResponse(`A password reset code has been sent to ${email} `).send(res);
    })
    .catch((err) => {
      throw new BadRequestError(err.message);
    });
});

exports.resetPassword = asyncHandler(async (req, res, next) => {
  const { code } = req.params;

  if (!code) {
    throw new BadRequestError('No code provided');
  }

  const user = await userModel.findOne({
    passwordResetToken: code,
    passwordResetExpires: { $gt: Date.now() },
  });
  if (!user) {
    throw new BadRequestError('Code is wrong or has expired');
  }

  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();
  createSendToken(user, res);
});

exports.refreshToken = asyncHandler(async (req, res) => {
  const refreshToken = req.body.token;

  if (!refreshToken || !refreshTokens.includes(refreshToken)) {
    throw new BadRequestError('Refresh token not found , please login again');
  }

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) throw new BadRequestError(err.message);
    const accessToken = signToken(user.id);
    return new SuccessResponse(accessToken).send(res);
  });
});

exports.verifyCode = asyncHandler(async (req, res) => {
  const { code } = req.body;

  if (!code) {
    throw new BadRequestError('No code provided');
  }

  const user = await userModel.findOne({
    passwordResetToken: code,
    passwordResetExpires: { $gt: Date.now() },
  });
  if (!user) {
    throw new BadRequestError('Code is wrong or has expired');
  }
  return new SuccessMsgResponse('Valid').send(res);
});
