const { SuccessMsgDataResponse, NotFoundResponse } = require("../middlewares/apiResponse");
const asyncHandler = require("express-async-handler");
const attchModel = require("../models/attchModel");
const compress = require("../lib/compression");
const { paginate } = require("../utils/apiFeatures")

exports.createAttch = asyncHandler(async (req,res) => {
    const { file } = req;
    const filePayload = {
        ...file,
        location:`http://localhost:5000/${file.filename}`
    }
    const newFile = await attchModel.create(filePayload);
    return new SuccessMsgDataResponse(newFile,"success").send(res)
});


exports.compress = asyncHandler(async (req,res) => {
    const { mediaId } = req.params;
    const foundImage = await attchModel.findById(mediaId);
    const { body } = req;
    if(!foundImage)
        throw new NotFoundResponse("image not found").send(res);
    const compressInfo = await compress(foundImage,body);
    const ret = await attchModel.findOneAndUpdate({_id:mediaId},{
        compressed:compressInfo
    },{
        new:true
    })
    res.json(ret)
})

exports.getAll = asyncHandler(async (req,res) => {
    return await paginate(req,res,attchModel)
})

exports.delete = asyncHandler(async (req,res) => {
    const { mediaId } = req.query;
    return await attchModel.deleteOne({_id:mediaId})
})

