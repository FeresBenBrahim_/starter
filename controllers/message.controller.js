const { SuccessMsgDataResponse, NotFoundResponse } = require('../middlewares/apiResponse');
const asyncHandler = require('express-async-handler');
const messageModel = require('../models/messageModel');
const { paginate } = require('../utils/apiFeatures');

exports.createMessage = asyncHandler(async (req, res) => {
  const createMessage = await messageModel.create(req.body);
  res.json(createMessage);
});
exports.updateMessage = asyncHandler(async (req, res) => {
  const { id } = req.params;

  const isExist = await messageModel.findById(id);
  if (!isExist) throw new Error('no message with that id');

  await messageModel.findByIdAndUpdate(id, req.body);
  res.json(createMessage);
});

exports.getAll = asyncHandler(async (req, res) => {
  const msgs = await paginate(req, res, messageModel);
  res.json(msgs);
});
exports.get = asyncHandler(async (req, res) => {
  const { id } = req.params;

  const msg = await messageModel.findOne({ createdBy: id });
  res.json(msg);
});

exports.delete = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const isExist = await messageModel.findById(id);
  if (!isExist) throw new Error('no message with that id');
  await messageModel.deleteOne({ _id: id });
  res.json({});
});
