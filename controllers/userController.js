const asyncHandler = require("express-async-handler");
const userModel = require("../models/userModel");
const ApiError = require("../middlewares/apiError");
const cloudinary = require("../utils/cloudinary");
const bcrypt = require("bcrypt");
const { BadRequestError, NotFoundError } = require("../middlewares/apiError");
const {
  SuccessMsgResponse,
  SuccessResponse,
  SuccessMsgDataResponse,
  SuccessResponsePagination,
} = require("../middlewares/apiResponse");

exports.getUser = asyncHandler(async (req, res) => {
  try {
    const user = await userModel.findById(req.params.id);
    if (!user) {
      throw new NotFoundError("No user found with that id");
    }
    return new SuccessResponse(user).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
exports.createUser = asyncHandler(async (req, res) => {
  try {
    const checkUser = await userModel.findOne({ email: req.body.email });
    if (checkUser) {
      throw new BadRequestError("A user with this email already exists");
    }
    if (req.file) {
      const result = await cloudinary.uploader.upload(req.file.path);
      req.body.avatar = result.secure_url || "";
      req.body.cloudinaryID = result.public_id || "";
    }
    const user = await userModel.create(req.body);
    return new SuccessMsgDataResponse(user, "User created successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
exports.getUsers = asyncHandler(async (req, res) => {
  try {
    const { page, limit } = req.query;
    const options = {
      page: parseInt(page, 10) || 1,
      limit: parseInt(limit, 10) || 10,
    };
    const users = await userModel.paginate({}, options);

    if (!users) {
      return new SuccessMsgResponse("No users found").send(res);
    }
    const { docs, ...meta } = users;

    return new SuccessResponsePagination(docs, meta).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});


exports.deleteUser = asyncHandler(async (req, res) => {
  try {
    const user = await userModel.findById(req.params.id);
    if (!user) {
      return next(new ApiError(404, "No user found."));
    }
    if (user?.avatar !== null) {
      await cloudinary.uploader.destroy(user?.cloudinaryID);
    }
    await userModel.findByIdAndDelete(req.params.id);
    return new SuccessMsgDataResponse(user, "User deleted successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
exports.updateUser = asyncHandler(async (req, res) => {
  try {
    const user = await userModel.findById(req.params.id);
    if (!user) {
      throw new BadRequestError("No user found with that id");
    }
    if (req.file && user?.avatar !== null) {
      await cloudinary.uploader.destroy(user?.cloudinaryID);
      const result = await cloudinary.uploader.upload(req.file.path);
      req.body.avatar = result?.secure_url || "";
      req.body.cloudinaryID = result?.public_id || "";
    }
    if (req.file && user?.avatar === null) {
      const result = await cloudinary.uploader.upload(req.file.path);
      req.body.avatar = result?.secure_url || "";
      req.body.cloudinaryID = result?.public_id || "";
    }
    
    const { email } = req.body;
    const checkUser = await userModel.findOne({ email });

    if (checkUser && checkUser.email !== user.email)
    {
      throw new BadRequestError('A user with this email already exists')
    }
    if (checkUser && checkUser.email === user.email) {
      delete req.body.email;
    }

    if (req.body.password) {
      req.body.password = await bcrypt.hash(req.body.password, 12);
      req.body.passwordConfirm = undefined;
    }

    const updatedUser = await userModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true }
    );
    return new SuccessMsgDataResponse(
      updatedUser,
      "User updated successfully"
    ).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.getMe = asyncHandler(async (req, res) => {
  try {
    let token;
    if (req.headers.authorization) {
      token = req.headers.authorization;
    }
    if (!token) {
      return next(
        new UnauthorizedError(
          "You are not logged in! Please log in to get access."
        )
      );
    }
    const decoded = await promisify(jwt.verify)(token, process.env.SECRET);
    const freshUser = await userModel.findById(decoded.id);
    if (!freshUser) {
      return next(
        new UnauthorizedError(
          "The user belonging to this token does no longer exist."
        )
      );
    }
    return new SuccessResponse(freshUser).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
