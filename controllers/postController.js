const asyncHandler = require("express-async-handler");
const postModel = require("../models/postModel");
const ApiError = require("../middlewares/apiError");
const { BadRequestError, NotFoundError } = require("../middlewares/apiError");
const {
  SuccessMsgResponse,
  SuccessResponse,
  SuccessMsgDataResponse,
  SuccessResponsePagination,
  NotFoundResponse,
} = require("../middlewares/apiResponse");

exports.getPost = asyncHandler(async (req, res) => {
  try {
    const post = await postModel.findById(req.params.id);
    if (!post) {
      throw new NotFoundError("No post found with that id");
    }
    return new SuccessResponse(post).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
exports.createPost = asyncHandler(async (req, res) => {
  try {
    const post = await postModel.create(req.body);
    return new SuccessMsgDataResponse(post, "Post created successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
exports.updatePost = asyncHandler(async (req, res) => {
  try {
    const old = await postModel.findById(req.params.id);
    if (!old) {
      return next(new ApiError(404, "No Post found with that id ."));
    }
    const post = await postModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true }
    );
    return new SuccessMsgDataResponse(post, "Post created successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
exports.deletePost = asyncHandler(async (req, res) => {
  try {
    const post = await postModel.findById(req.params.id);
    if (!post) {
      return next(new ApiError(404, "No Post found with that id ."));
    }
    await postModel.findByIdAndDelete(req.params.id);
    return new SuccessMsgDataResponse(post, "Post deleted successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.getPosts = asyncHandler(async (req, res) => {
  try {
    const { page, limit } = req.query;
    const options = {
      page: parseInt(page, 10) || 1,
      limit: parseInt(limit, 10) || 10,
    };
    const posts = await postModel.paginate({}, options);

    if (!posts) {
      return new SuccessMsgResponse("No posts found").send(res);
    }
    const { docs, ...meta } = posts;

    return new SuccessResponsePagination(docs, meta).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.upPost = asyncHandler(async (req, res) => {
  const postId = req.params.id;
  let token;
  if (req.headers.authorization) {
    token = req.headers.authorization;
  }
  if (!token) {
    return next(
      new UnauthorizedError(
        "You are not logged in! Please log in to get access."
      )
    );
  }
  const decoded = await promisify(jwt.verify)(token, process.env.SECRET);
  const userId = decoded.id;
  try {
    const old = await postModel.findById(postId);
    if (!old) {
      return next(new ApiError(404, "No Post found with that id ."));
    }
    old.up += 1;
    old.history.push({ user: userId, choice: "up" });
    await old.save();
    res.status(200).json({ message: "Reacted successfully", post });
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.downPost = asyncHandler(async (req, res) => {
  const postId = req.params.id;

  let token;
  if (req.headers.authorization) {
    token = req.headers.authorization;
  }
  if (!token) {
    return next(
      new UnauthorizedError(
        "You are not logged in! Please log in to get access."
      )
    );
  }
  const decoded = await promisify(jwt.verify)(token, process.env.SECRET);
  const userId = decoded.id;
  try {
    const old = await postModel.findById(postId);
    if (!old) {
      return next(new ApiError(404, "No Post found with that id ."));
    }
    old.down += 1;
    old.history.push({ user: userId, choice: "down" });
    await old.save();
    res.status(200).json({ message: "Reacted successfully", post });
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
