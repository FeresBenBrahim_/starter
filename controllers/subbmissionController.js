const asyncHandler = require("express-async-handler");
const submissionModel = require("../models/submissionModel");
const ApiError = require("../middlewares/apiError");
const { BadRequestError, NotFoundError } = require("../middlewares/apiError");
const {
  SuccessMsgResponse,
  SuccessResponse,
  SuccessMsgDataResponse,
  SuccessResponsePagination
} = require("../middlewares/apiResponse");

exports.getSubmission = asyncHandler(async (req, res) => {
  try {
    const submission = await submissionModel.findById(req.params.id);
    if (!submission) {
      throw new NotFoundError("No submission found with that id");
    }
    return new SuccessResponse(submission).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.createSubmission = asyncHandler(async (req, res) => {
  try {
    const submission = await submissionModel.create(req.body);
    return new SuccessMsgDataResponse(submission, "Submission created successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.updateSubmission = asyncHandler(async (req, res) => {
  try {
    const old = await submissionModel.findById(req.params.id);
    if (!old) {
      return next(new ApiError(404, "No Submission found with that id ."));
    }
    const submission = await submissionModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true }
    );
    return new SuccessMsgDataResponse(submission, "Submission created successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.giveCertificate = asyncHandler(async(req,res)=>{
  const submissionId = req.params.id;

  try {
    const old = await submissionModel.findById(submissionId);
    if (!old) {
      return next(new ApiError(404, "No Post found with that id ."));
    }
    old.certified=true
    await old.save();
    res.status(200).json({ message: "Certificate Given successfully", submission });
  } catch (err) {
    throw new BadRequestError(err.message);
  }
})

exports.removeCertificate = asyncHandler(async(req,res)=>{
  const submissionId = req.params.id;
  try {
    const old = await submissionModel.findById(submissionId);
    if (!old) {
      return next(new ApiError(404, "No Submission found with that id ."));
    }
    old.certified=false
    await old.save();
    res.status(200).json({ message: "Certificate Remove successfully", old });
  } catch (err) {
    throw new BadRequestError(err.message);
  }
})

exports.deleteSubmission = asyncHandler(async (req, res) => {
  try {
    const submission = await submissionModel.findById(req.params.id);
    if (!submission) {
      return next(new ApiError(404, "No Submission found with that id ."));
    }
    await submissionModel.findByIdAndDelete(req.params.id);
    return new SuccessMsgDataResponse(submission, "Submission deleted successfully").send(
      res
    );
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});

exports.getSubmissions = asyncHandler(async (req, res) => {
  try {
    const { page, limit } = req.query;
    const options = {
      page: parseInt(page, 10) || 1,
      limit: parseInt(limit, 10) || 10,
    };
    const submissions = await submissionModel.paginate({}, options);

    if (!submissions) {
      return new SuccessMsgResponse("No submissions found").send(res);
    }
    const { docs, ...meta } = submissions;

    return new SuccessResponsePagination(docs, meta).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
});
