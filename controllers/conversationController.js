const { SuccessMsgDataResponse, NotFoundResponse } = require('../middlewares/apiResponse');
const asyncHandler = require('express-async-handler');
const conversationModel = require('../models/conversationModel');
const { paginate } = require('../utils/apiFeatures');

exports.createConversation = asyncHandler(async (req, res) => {
  const conversation = await conversationModel.create({
    createdBy: req?.user?._id,
  });

  res.json({
    status: 'success',
    body: conversation,
  });
});
exports.updateConversation = asyncHandler(async (req, res) => {
  const { id } = req.query;

  const isExist = await conversationModel.findById(id);
  if (!isExist) throw new Error('no message with that id');

  const updatedConv = await conversationModel.findByIdAndUpdate(id, req.body);
  res.json({ status: 'success', body: updatedConv });
});

exports.get = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const conversation = await conversationModel.findOne({
    createdBy: req?.user?._id,
    _id: id,
  });

  if (!conversation) {
    return res.status(404).json({ message: 'Conversation not found ' });
  }
  return res.status(200).json({ data: conversation });
});

exports.delete = asyncHandler(async (req, res) => {
  const { id } = req.query;
  const isExist = await conversationModel.findById(id);
  if (!isExist) throw new Error('no message with that id');
  await conversationModel.deleteOne({ _id: id });
  res.json({});
});
