const authController = require("../../controllers/authController");
const { schemaValidator } = require("../../middlewares/schemaValidator");
const {
  signup,
  login,
  updatePassword,
  updateMe,
  resetPassword,
  forgotPassword,
  verifyCode
} = require("./schemas/authSchemas");

const express = require("express");
const router = express.Router();
/**
 * @swagger
 * tags:
 *   name: User
 *   description: The User managing API
 */


/**
 * @swagger
 * /register:
 *   post:
 *     summary: Register
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The list of the register
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/User'
 *
 *
 */

router.post("/register", schemaValidator(signup),authController.uploadFile, authController.signUp);

/**
 * @swagger
 * /login:
 *   post:
 *     summary: User login
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/LoginSchema'
 *     tags: [Auth]
 *     responses:
 *       200:
 *         description: Login
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/LoginSchema'
 *
 */

 router.post("/login", schemaValidator(login), authController.login);

/**
 * @swagger
 * /me:
 *   get:
 *     summary: Get me
 *     tags: [Auth]
 *     responses:
 *       200:
 *         description: Get me
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/User'
 *     security:
 *      - bearerAuth: []
 */

 router.route("/me").get(authController.getMe);

/**
 * @swagger
 * /updateMyPassword:
 *   put:
 *     summary: Password update
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/Password'
 *     responses:
 *       200:
 *         description: Password update
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/Password'
 *     security:
 *      - bearerAuth: []
 */

 router
 .route("/updateMyPassword")
 .put(
   authController.protect,
   schemaValidator(updatePassword),
   authController.updatePassword
 );

/**
 * @swagger
 * /updateMe:
 *   put:
 *     summary: Update user's profile
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: Update user's profile
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/User'
 *     security:
 *      - bearerAuth: []
 */
 router
 .route("/updateMe")
 .put(
   authController.protect,
   schemaValidator(updateMe),
   authController.uploadFile,
   authController.updateMe
 );

 /**
 * @swagger
 * /forgotPassword:
 *   post:
 *     summary: Forgot password
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *              properties:
 *                email:
 *                  type: string
 *     responses:
 *       200:
 *         description: A token will be sent to your email
 *     security:
 *      - bearerAuth: []
 */

 router.post('/forgotPassword', schemaValidator(forgotPassword), authController.forgotPassword);

  /**
 * @swagger
 * /resetPassword/{code}:
 *   put:
 *     summary: Reset password
 *     tags: [Auth]
 *     parameters:
 *      - in: path
 *        name: code
 *        required: true
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - password
 *                - passwordConfirm
 *              properties:
 *                password:
 *                  type: string
 *                passwordConfirm:
 *                  type: string
 *     responses:
 *       200:
 *         description: Password has been reset successfully
 *     security:
 *      - bearerAuth: []
 */

 router.put('/resetPassword/:code',schemaValidator(verifyCode,"params"), schemaValidator(resetPassword), authController.resetPassword);
  /**
 * @swagger
 * /verifyCode:
 *   post:
 *     summary: Verify code
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - code
 *              properties:
 *                code:
 *                  type: string
 *     responses:
 *       200:
 *         description: Code verification
 *     security:
 *      - bearerAuth: []
 */

 router.post('/verifyCode', schemaValidator(verifyCode), authController.verifyCode);

  /**
 * @swagger
 * /refreshToken:
 *   post:
 *     summary: Reset token
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - token
 *              properties:
 *                token:
 *                  type: string
 *     responses:
 *       200:
 *         description: Refresh access token
 *     security:
 *      - bearerAuth: []
 */

 router.post('/refreshToken', authController.refreshToken);


  /**
 * @swagger
 * /create/admin:
 *   post:
 *     summary: Reset token
 *     tags: [Auth]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - token
 *              properties:
 *                token:
 *                  type: string
 *     responses:
 *       200:
 *         description: Refresh access token
 *     security:
 *      - bearerAuth: []
 */
router.post("/create/admin",authController.restrictTo("admin"), schemaValidator(signup), authController.createAdmin);

module.exports = router;
