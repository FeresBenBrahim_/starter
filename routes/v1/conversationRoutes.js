const express = require('express');
const router = express.Router();
const conversationController = require('../../controllers/conversationController');
const authController = require('../../controllers/authController');
const schema = require('./schemas/conversationSchemas');
const { schemaValidator } = require('../../middlewares/schemaValidator');

router.route('/:id').get(conversationController.get);

router.post('/', conversationController.createConversation);

router.put(
  '/:id',

  conversationController.updateConversation
);

router.delete(
  '/:id',

  conversationController.delete
);

module.exports = router;
