const router = express.Router();
const postController = require("../../controllers/postController");
const { create , update } = require("./schemas/postSchema");
const authController = require("../../controllers/authController");
const { schemaValidator } = require("../../middlewares/schemaValidator");

router.post("/", authController.protect, schemaValidator(create), postController.createPost);
router.delete("/:id", authController.protect, postController.deletePost);
router.get("/", authController.protect, postController.getPosts);
router.get("/:id", authController.protect, postController.getPost);
router.patch("/", authController.protect,schemaValidator(update), postController.updatePost);
router.post("/:id/up", authController.protect, postController.upPost);
router.post("/:id/down", authController.protect, postController.downPost);
