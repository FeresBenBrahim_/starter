const express = require('express');
const router = express.Router();
const messageController = require('../../controllers/message.controller');
const authController = require('../../controllers/authController');

router.get('/', authController.protect, messageController.getAll);

router.route('/:id').get(messageController.get);

router.post(
  '/',

  messageController.createMessage
);

router.put(
  '/:id',

  messageController.updateMessage
);

router.delete(
  '/:id',

  messageController.delete
);

module.exports = router;
