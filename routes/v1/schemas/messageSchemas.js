const Joi = require('joi');
const { JoiObjectId } = require('../../../middlewares/schemaValidator');

exports.create = Joi.object().keys({
  input: Joi.string(),
  fromUser: Joi.boolean(),
  fromBot: Joi.boolean(),
  conversation: Joi.array().items(JoiObjectId()).min(1),
});

exports.update = Joi.object().keys({
  input: Joi.string().optional(),
  fromUser: Joi.boolean().optional(),
  fromBot: Joi.boolean().optional(),
  conversation: Joi.array().items(JoiObjectId()).optional(),
});
