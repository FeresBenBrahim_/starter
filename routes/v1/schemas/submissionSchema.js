exports.create = Joi.object().keys({
    id: Joi.string().min(3).max(30).required(),
    name: Joi.string().min(3).required(),
    field: Joi.string().min(3).required(),
    address: Joi.string().min(3).required(),
    certificateType: Joi.string().min(3).required(),

});

exports.update = Joi.object().keys({
    id: Joi.string().min(3).max(30).optional(),
    name: Joi.string().min(3).optional(),
    field: Joi.string().min(3).optional(),
    address: Joi.string().min(3).optional(),
    certificateType: Joi.string().min(3).optional(),

});