


exports.create = Joi.object().keys({
    title: Joi.string().min(3).max(30).required(),
    description: Joi.string().min(3).required()
});

exports.update = Joi.object().keys({
    title: Joi.string().min(3).max(30).optional(),
    description: Joi.string().min(3).optional()
});