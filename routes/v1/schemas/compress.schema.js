const Joi = require("joi");

exports.imageSchema = Joi.array().items(Joi.object().keys({
    rotate:Joi.number().optional().default(90),
    extTarget:Joi.string().optional().default("jpeg"),
    resize:Joi.object().keys({
      width:Joi.number().optional().default(800),
      height:Joi.number().optional().default(600)
    }).optional()
})).optional()