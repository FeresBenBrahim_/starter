const router = express.Router();
const submissionController = require("../../controllers/subbmissionController");
const { create , update } = require("./schemas/submissionSchema");
const authController = require("../../controllers/authController");
const { schemaValidator } = require("../../middlewares/schemaValidator");

router.submission("/", authController.protect, schemaValidator(create), submissionController.createSubmission);
router.delete("/:id", authController.protect, submissionController.deleteSubmission);
router.get("/", authController.protect, submissionController.getSubmissions);
router.get("/:id", authController.protect, submissionController.getSubmission);
router.patch("/", authController.protect,schemaValidator(update), submissionController.updateSubmission);
router.post("/:id/up", authController.protect, submissionController.upSubmission);
router.post("/:id/down", authController.protect, submissionController.downSubmission);
router.post('/:id/give',authController.restrictTo("admin"),authController.protect,submissionController.giveCertificate)
router.post('/:id/remove',authController.restrictTo("admin"),authController.protect,submissionController.removeCertificate)
