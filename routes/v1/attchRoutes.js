const express = require("express");
const router = express.Router();
const multer  = require('multer')
const attchController = require("../../controllers/attch.controller");
const { imageSchema } = require("./schemas/compress.schema");
const { schemaValidator } = require("../../middlewares/schemaValidator");


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
      cb(null, uniqueSuffix+"-"+file.originalname)
    }
  })
  
const upload = multer({ storage: storage })
  

router.post("/",upload.single("file"),attchController.createAttch);

router.post("/compress/:mediaId",
            schemaValidator(imageSchema),
            attchController.compress
);

router.get("/",attchController.getAll);
module.exports = router;