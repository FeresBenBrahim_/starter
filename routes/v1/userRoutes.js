const express = require("express");
const router = express.Router();
const userController = require("../../controllers/userController");
const authController = require("../../controllers/authController");
const {
  createUser,
  updateUser,
  checkUserId,
} = require("./schemas/userSchemas");
const { schemaValidator } = require("../../middlewares/schemaValidator");

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Returns the list of all the users
 *     tags: [User]
 *     parameters:
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *     responses:
 *       200:
 *         description: The list of the users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       400:
 *        description: 	Validation Failed
 *       401:
 *        description: Error Token
 *       403:
 *        description: Access Denied / Unauthorized
 *       404:
 *        description: Not found
 *       500:
 *        description: Internal server error
 *     security:
 *      - bearerAuth: []
 */

 router.get(
  "/",
  authController.protect,
  userController.getUsers
);


/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get one user by id
 *     tags: [User]
 *     parameters:
 *      - in: path
 *        name: id
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       400:
 *        description: 	Validation Failed
 *       401:
 *        description: Error Token
 *       403:
 *        description: Access Denied / Unauthorized
 *       404:
 *        description: Not found
 *       500:
 *        description: Internal server error
 *     security:
 *      - bearerAuth: []
 */

 router
 .route("/:id")
 .get(
   authController.protect,
   authController.restrictTo("admin"),
   schemaValidator(checkUserId, "params"),
   userController.getUser
 );


/**
 * @swagger
 * /users:
 *   post:
 *     summary: User Creation
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/User'
 *     tags: [User]
 *     responses:
 *       200:
 *         description: User creation
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       400:
 *        description: 	Validation Failed
 *       401:
 *        description: Error Token
 *       403:
 *        description: Access Denied / Unauthorized
 *       404:
 *        description: Not found
 *       500:
 *        description: Internal server error
 *     security:
 *      - bearerAuth: []
 *
 */

 router.post(
  "/",
  authController.protect,
  authController.restrictTo("admin"),
  authController.uploadFile,
  schemaValidator(createUser),
  userController.createUser
);


/**
 * @swagger
 * /users/{id}:
 *   put:
 *     summary: update one user by id
 *     tags: [User]
 *     parameters:
 *      - in: path
 *        name: id
 *     requestBody:
 *         required: true
 *         content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       400:
 *        description: 	Validation Failed
 *       401:
 *        description: Error Token
 *       403:
 *        description: Access Denied / Unauthorized
 *       404:
 *        description: Not found
 *       500:
 *        description: Internal server error
 *     security:
 *      - bearerAuth: []
 */

 router.put(
  "/:id",
  authController.protect,
  authController.restrictTo("admin"),
  schemaValidator(checkUserId, "params"),
  authController.uploadFile,
  schemaValidator(updateUser),
  userController.updateUser
);


/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     summary: delete one user by id
 *     tags: [User]
 *     parameters:
 *      - in: path
 *        name: id
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       400:
 *        description: 	Validation Failed
 *       401:
 *        description: Error Token
 *       403:
 *        description: Access Denied / Unauthorized
 *       404:
 *        description: Not found
 *       500:
 *        description: Internal server error
 *     security:
 *      - bearerAuth: []
 */

 router.delete(
  "/:id",
  authController.protect,
  authController.restrictTo("admin"),
  schemaValidator(checkUserId, "params"),
  userController.deleteUser
);

module.exports = router;
