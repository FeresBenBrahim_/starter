const mongoose=require('mongoose');
const db=process.env.DB.replace('<password>',process.env.DB_PASSWORD);
const connect=async()=>{
    try {
        const conn=await mongoose.connect (db,{
            useNewUrlParser:true,
            useUnifiedTopology:true
        })   
        console.log('Mongo db connected', conn.connection.host)
    } catch (error) {
        console.log(error)
        process.exit(1)
    }
}


module.exports=connect