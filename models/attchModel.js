const { default: mongoose } = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');


const attchSchema = new mongoose.Schema({
  fieldname: String,
  originalname: String,
  encoding: String,
  mimetype: String,
  destination: String,
  filename: String,
  path: String,
  size: Number,
  location:String,
  compressed:{type:mongoose.Schema.Types.Mixed}
})

attchSchema.plugin(mongoosePaginate)

module.exports = mongoose.model("attachments",attchSchema);