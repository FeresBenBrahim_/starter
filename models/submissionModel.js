const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");


const subbmissionSchema = new mongoose.Schema(
  {
    id: {
      type: String,
    },
    name: {
      type: String,
    },
    field: {
      type: Number,
      default:0
    },
    address: {
      type: Number,
      default:0
    },
    certificateType:{
      submissionModeltype: String,
    },
    certified:{
      type: Boolean,
      default:false
    }
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

subbmissionSchema.plugin(mongoosePaginate);

subbmissionSchema.pre(/^find/, function (next) {
  this.select("");
  next();
});

module.exports = mongoose.model("subbmission", subbmissionSchema);
