const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const messageSchema = new mongoose.Schema({
  input: String,
  fromUser: Boolean,
  fromBot: Boolean,
  conversation: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Conversation',
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
  },
});

messageSchema.pre(/^find/, function (next) {
  this.populate({ path: 'conversation createdBy' });
  next();
});

messageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Message', messageSchema);
