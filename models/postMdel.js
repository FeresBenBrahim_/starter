const { Schema } = require("mongoose");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");


const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
    },
    description: {
      type: String,
    },
    up: {
      type: Number,
      default:0
    },
    down: {
      type: Number,
      default:0
    },
    history:[{
      user:{type:Schema.Types.ObjectId , ref: 'User'},
      choice:{type: String , enum: ["up", "down"],}
    }]
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

postSchema.plugin(mongoosePaginate);

postSchema.pre(/^find/, function (next) {
  this.select("");
  next();
});

module.exports = mongoose.model("Post", postSchema);
