const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const conversationSchema = new mongoose.Schema(
  {
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Users',
    },
  },
  { timestamps: true }
);

conversationSchema.pre(/^find/, function (next) {
  this.populate({ path: 'createdBy' });
  next();
});
conversationSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Conversation', conversationSchema);
