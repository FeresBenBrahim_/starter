# Use an official Node.js LTS image based on Debian as the base image
FROM node:14-buster

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Install additional dependencies for sharp (if using Alpine)


# Copy the rest of the application code
COPY . .

# Expose the port on which your application will run
EXPOSE 3000

# Command to run your application
CMD [ "npm", "start" ]
