const { BadRequestError } = require('../middlewares/apiError');
const { SuccessResponsePagination, SuccessMsgResponse } = require('../middlewares/apiResponse');

class APIFeatures {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  filter() {
    const queryObj = { ...this.queryString };
    const excludedFields = ['page', 'sort', 'limit', 'fields'];
    excludedFields.forEach((el) => delete queryObj[el]);

    // 1B) Advanced filtering
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    this.query = this.query.find(JSON.parse(queryStr));

    return this;
  }

  sort() {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(',').join(' ');
      this.query = this.query.sort(sortBy);
    } else {
      this.query = this.query.sort('-createdAt');
    }

    return this;
  }

  limitFields() {
    if (this.queryString.fields) {
      const fields = this.queryString.fields.split(',').join(' ');
      this.query = this.query.select(fields);
    } else {
      this.query = this.query.select('-__v');
    }

    return this;
  }
}

const paginate = async (req, res, model) => {
  try {
    console.log(model);
    const { page, limit } = req.query;

    const queryObj = req.query;
    const excludedFields = ['page', 'sort', 'limit', 'fields'];
    excludedFields.forEach((el) => delete queryObj[el]);

    // 1B) Advanced filtering
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    const options = {
      page: parseInt(page, 10) || 1,
      limit: parseInt(limit, 10) || 10,
    };
    const ret = await model.paginate(JSON.parse(queryStr), options);

    if (!ret) {
      return new SuccessMsgResponse('No users found').send(res);
    }
    const { docs, ...meta } = ret;
    return new SuccessResponsePagination(docs, meta).send(res);
  } catch (err) {
    throw new BadRequestError(err.message);s
  }
};

module.exports = { APIFeatures, paginate };
