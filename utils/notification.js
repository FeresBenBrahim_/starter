const { JWT } = require("google-auth-library");
const key = require ("../takifoot-a842f-firebase-adminsdk-n45n2-0541909eda.json");
const axios = require('axios');
const getAccessToken= function() {
  return new Promise(function (resolve, reject) {
    const jwtClient = new JWT(
      key.client_email,
      null,
      key.private_key,
      ["https://www.googleapis.com/auth/firebase.messaging"],
      null
    );
    jwtClient.authorize(function (err, tokens) {
      if (err) {
        reject(err);
        return;
      }
      resolve(tokens.access_token);
    });
  });
}

const sendNotif = function(
  token,
  { title, body, data = { users: [] }, topic = "all" }
) {
  const headers = {
    Authorization: "Bearer " + token,
  };
  const payload = {
    validate_only: false,
    message: {
      topic,
      notification: {
        title,
        body,
      },
      data: {
        users: "ALL",
      },
    },
  };
  const appId = process.env.FIRE_BASE_APP_ID;
  return axios({
    method: "post",
    url: `https://fcm.googleapis.com/v1/projects/${appId}/messages:send`,
    data: payload,
    headers,
  });
}
module.exports = {getAccessToken,sendNotif}