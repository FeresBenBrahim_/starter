// MULTER CONFIG

const multer = require("multer");
const path = require("path");

module.exports = multer({
  storage: multer.diskStorage({dest: 'uploads/'}),
  fileFilter: (req, file, cb) => {
    cb(null, true);
  },
});